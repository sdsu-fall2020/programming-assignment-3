
# Header comment section (typical assignment info)
import sys
# This function begins execution of program.
# Verify data input filename provided on command line: len(sys.argv)
# If error, output message for user: Usage: p3.py dataFileName'
# and quit, using sys.exit()
#
# Declare A, B, call read_matrices to initialize A, B, and store
# return value as C
#
# Print A and B contents
#
# Call mult_matrices
#
# Print result contents
#
def main():
    A = []
    B = []
    C = read_matrices(A, B)

    mult_matrices(A, B, C)

    print("Matrix A contents:")
    print_matrix(A)

    print("\nMatrix B contents:")
    print_matrix(B)

    print("\nMatrix A * B is:")
    print_matrix(C)

# This function reads m, n, and p from the datafile.
# Then matrices A and B are filled from the datafile.
# Matrix C is then allocated size m x p.
# The values for m, n, and p are local values filled in by this function
# PARAMETERS in order are:
# list matrix A
# list matrix B
# RETURN matrix C
#
def read_matrices(A,B):
    m = 0
    n = 0
    p = 0

    with open('myInputFile.dat') as f:
        m = int(next(f))
        n = int(next(f))
        p = int(next(f))
        #read A
        for i in range(m):
            A.append([int(x) for x in next(f).split()])
        #read B
        for i in range(n):
            B.append([int(x) for x in next(f).split()])

    #populate C
    C = []
    for i in range(m):
        C.append([0 for x in range(p)])
    return C


# This function prints a matrix. Rows and columns should be preserved.
# PARAMETERS in order are:
# list The matrix to print
#
def print_matrix(matrix):
    for i in matrix:
        print( "\t".join([str(x) for x in i]) )


# The two matrices A and B are multiplied, and matrix C contains the
# result.
# PARAMETERS in order are:
# list Matrix A
# list Matrix B
# list Matrix C (all zeros at this point)
#
def mult_matrices(A,B,C):
    m = len(A)
    n = len(B)
    p = len(C[0])
    for i in range(m):
        for j in  range(p):
            for k in range(n):
                C[i][j] += A[i][k] * B[k][j]

# Begin program
if __name__ == '__main__':
    main()